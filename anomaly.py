# -*- coding: utf-8 -*-
"""
Created on Tue Jan 22 10:21:41 2019

@author: US16120
"""

import pandas as pd
from datetime import datetime
import numpy as np

df = pd.read_excel('tags_for_paul.xlsx')
df.reset_index(level=0, inplace=True)
df = df.rename(index=str, columns={"index": "Date"})
df.head()

df['datetime'] = pd.to_datetime(df['Date'])
df = df.set_index('datetime')
df.drop(['Date'], axis=1, inplace=True)
df.head()

import luminol
from luminol.anomaly_detector import AnomalyDetector

non_rev_df = df["NON_REV"]
non_rev_df.head()

detector = AnomalyDetector(non_rev_df)


ts = {0: 0, 1: 0.5, 2: 1, 3: 1, 4: 1, 5: 0, 6: 0, 7:0, 8:0}

q = non_rev_df.to_dict()

my_detector = AnomalyDetector(q)
score = my_detector.get_all_scores()
for timestamp, value in score.iteritems():
    print(timestamp, value)
    
    
ts = pd.Series()

%matplotlib inline
import matplotlib.pyplot as plt
import seaborn; seaborn.set()

qaz = df["NON_REV"]

qaz.plot()

df.index = pd.DatetimeIndex(df.index)

idx = pd.date_range('01-01-2017', '01-01-2018', freq='H')

#####

df = pd.read_excel('tags_for_paul.xlsx')
df.reset_index(level=0, inplace=True)
df = df.rename(index=str, columns={"index": "Date"})
df.head()
non_rev_df = df[['Date', 'NON_REV']]

idx = pd.date_range('01-01-2017', '12-31-2017', freq='H')
qaz = idx.to_frame(index = False)
qaz.columns = ['Date']
qaz['tag'] = "sequential"
qaz.head()

zxc = pd.merge(non_rev_df, qaz, how = 'outer', on = 'Date')

qw = non_rev_df.duplicated('Date')
