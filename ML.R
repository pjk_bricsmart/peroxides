library(tidyverse)
library(magrittr)
library(caret)
library(readxl)
library(DataExplorer)
library(tableplot)
library(ggplot2)
library(ggthemes)
library(janitor)
library(naniar)
library(readr)
library(randomForest)
library(rpart)
library(rpart.plot)

DATA <- read_xlsx(path = 'tags_for_paul.xlsx', col_names = TRUE) %>%
  rename(Date = X__1) %>%
  clean_names() %>%
  remove_empty(which = c("rows","cols"))
dim(DATA)

# vis_miss(DATA[, 1:20])
# vis_miss(DATA[, 21:40])
# vis_miss(DATA[, 41:60])
# vis_miss(DATA[, 61:80])
# vis_miss(DATA[, 81:100])
# vis_miss(DATA[, 101:120])
# vis_miss(DATA[, 121:140])
# vis_miss(DATA[, 141:160])
# vis_miss(DATA[, 161:180])
# vis_miss(DATA[, 181:196])

NAME <- names(DATA)

excludeList01 <- NULL
for (k in 1:length(NAME)) {
  if( length(table(DATA[[k]])) == 1 ) {
    print(NAME[[k]])
    excludeList01 <- c(excludeList01, NAME[[k]])
  }
}

DATA2 <- DATA %>% select(-excludeList01)
dim(DATA2)
NAME2 <- names(DATA2)

# vis_miss(DATA2[, 1:20])
# vis_miss(DATA2[, 21:40])
# vis_miss(DATA2[, 41:60])
# vis_miss(DATA2[, 61:80])
# vis_miss(DATA2[, 81:100])
# vis_miss(DATA2[, 101:120])
# vis_miss(DATA2[, 121:140])
# vis_miss(DATA2[, 141:160])
# vis_miss(DATA2[, 161:178])

excludeList02 <- NULL
for (k in 1:length(NAME2)) {
  n <- DATA2 %>%
    select(date, NAME2[[k]]) %>%
    na.omit()
  if( max(n$date) < max(DATA2$date) ) {
    print(NAME2[[k]])
    excludeList02 <- c(excludeList02, NAME2[[k]])
  }
}

DATA3 <- DATA2 %>% select(-excludeList02)
dim(DATA3)
NAME3 <- names(DATA3)

# vis_miss(DATA3[, 1:20])
# vis_miss(DATA3[, 21:40])
# vis_miss(DATA3[, 41:60])
# vis_miss(DATA3[, 61:80])
# vis_miss(DATA3[, 81:100])
# vis_miss(DATA3[, 101:120])
# vis_miss(DATA3[, 121:140])
# vis_miss(DATA3[, 141:161])

excludeList03 <- NULL
for (k in 1:nrow(DATA3)) {
  if( sum(is.na(DATA3[k,])) != 0 ) {
    excludeList03 <- c(excludeList03, DATA3[k, 1])
  }
}

'%!in%' <- function(x,y)!('%in%'(x,y))
DATA4 <- DATA3 %>% filter(date %!in% excludeList03)
dim(DATA4)

dim(na.omit(DATA4))

# DATA5 <- read.csv('tags-curated.csv', stringsAsFactors = FALSE, header = TRUE) %>%
#   select(-X)

DATA4 <- DATA4 %>%
  data.frame()

# A. Emery: remove variable starting with "f5507"
# DATA5 <- DATA4 %>% select(-starts_with("f5507"))
# NON_REV =
#   DK2 ... F5507_Residue
#  -CS2 ... F5507_AQ
#  -DW2 ... F5507_TQ
#  -DU2 ... F5507_TEQ
#  -DO2 ... F5507_Sum_C_T_THQ
#  -DC2 ... F5507_OX
#  -CP2 ... F5507_AA_DP

DATA5 <-
  DATA4 %>% select(
    -f5507_residue,
    -f5507_aq,
    -f5507_tq,
    -f5507_teq,
    -f5507_sum_c_t_thq,
    -f5507_ox,
    -f5507_aa_dp
  )

set.seed(350)

zxc <- DATA5 %>%
  mutate(tranche = ntile(DATA5$non_rev, 50)) %>%
  select(tranche) %>%
  data.frame()
trainingRows <- createDataPartition(zxc$tranche,
                                    p = 0.80,
                                    groups = 10,
                                    list = FALSE)

train <- DATA5[trainingRows, ]
test <- DATA5[-trainingRows, ]

t_rain <- train %>%
  mutate(group = 'train')
t_est <- test %>%
  mutate(group = 'test')
alles <- rbind(t_rain, t_est)
rm(t_rain, t_est)
ggplot(alles, aes(non_rev, stat(density), colour = group)) +
  geom_freqpoly(binwidth = 0.5) +
  ggthemes::theme_tufte()

trainNoDate <- train %>%
  select(-date)
comboInfo <- findLinearCombos(trainNoDate)
# > comboInfo$remove
# [1] 95 ... "f5507_saq"
train <- train[ , -comboInfo$remove]
test <- test[ , -comboInfo$remove]

X_train <- train %>%
  select(-date, -non_rev)
y_train <- train %>%
  select(non_rev)

X_test <- test %>%
  select(-date, -non_rev)
y_test <- test %>%
  select(non_rev)

# curate data

## near-zero variance descriptors

nzv <- nearZeroVar(X_train, freqCut = 100/0)
# (empty)

## highly correlated descriptors

correlations <- cor(X_train)
corrplot::corrplot(correlations, order = 'hclust')
highCorr <- findCorrelation(correlations, cutoff = 0.85)

names(X_train[ , highCorr])

X_train <- X_train[ , -highCorr]
X_test <- X_test[ , -highCorr]

correlations <- cor(X_train)
corrplot::corrplot(correlations, order = 'hclust')

## center & scale descriptors

preProcValues <- preProcess(X_train, method = c('center', 'scale'))

X_train_proc <- predict(preProcValues, X_train)
X_test_proc <- predict(preProcValues, X_test)

# models

## random forest

data2model <- cbind(X_train, y_train)

rfModel <- randomForest(non_rev ~ ., data = data2model, importance = TRUE)
rfModel

varImpPlot(rfModel, type = 1, cex = 0.8)

y_predict <- predict(rfModel, newdata = X_test) %>%
  data.frame()
colnames(y_predict) <- c('Predicted')

data2plot <- cbind(y_test, y_predict)

summary(lm(non_rev ~ Predicted, data = data2plot))

p <-
  ggplot(data2plot, aes(Predicted, non_rev)) +
  geom_point(colour = "blue", size = 2) +
  coord_equal() +
  # xlim(c(0, 3.5)) + ylim(c(0, 3.5)) +
  geom_smooth(method = 'lm') +
  labs(title = 'non_rev',
       subtitle = 'Random Forest\n test data\n all 120 variables') +
  ggthemes::theme_tufte()
p <- p + geom_abline(intercept = 0,
                     slope = 1,
                     colour = 'red')
p

## fitting with fewer variables

### 25 most important variables

varImp <- importance(rfModel)
importantVars <- names(sort(varImp[ , 1], decreasing = TRUE))[1:25]
X_train_imp <- X_train[ , importantVars]
X_test_imp <- X_test[ , importantVars]

data2model_imp <- cbind(X_train_imp, y_train)

rfModel_imp_25 <- randomForest(non_rev ~ ., data = data2model_imp, importance = TRUE)
rfModel_imp_25

y_predict_imp_25 <- predict(rfModel_imp_25, newdata = X_test_imp) %>%
  data.frame()
colnames(y_predict_imp_25) <- c('Predicted')

data2plot_imp_25 <- cbind(y_test, y_predict_imp_25)

summary(lm(non_rev ~ Predicted, data = data2plot_imp_25))

p_imp_25 <-
  ggplot(data2plot_imp_25, aes(Predicted, non_rev)) +
  geom_point(colour = "blue", size = 2) +
  coord_equal() +
  # xlim(c(0, 3.5)) + ylim(c(0, 3.5)) +
  geom_smooth(method = 'lm') +
  labs(title = 'non_rev',
       subtitle = 'Random Forest\n test data\n varImp = 25') +
  ggthemes::theme_tufte()
p_imp_25 <- p_imp_25 + geom_abline(intercept = 0,
                             slope = 1,
                             colour = 'red')
p_imp_25

### 20 most important variables

varImp <- importance(rfModel)
importantVars <- names(sort(varImp[ , 1], decreasing = TRUE))[1:20]
X_train_imp <- X_train[ , importantVars]
X_test_imp <- X_test[ , importantVars]

data2model_imp <- cbind(X_train_imp, y_train)

rfModel_imp_20 <- randomForest(non_rev ~ ., data = data2model_imp, importance = TRUE)
rfModel_imp_20

y_predict_imp_20 <- predict(rfModel_imp_20, newdata = X_test_imp) %>%
  data.frame()
colnames(y_predict_imp_20) <- c('Predicted')

data2plot_imp_20 <- cbind(y_test, y_predict_imp_20)

summary(lm(non_rev ~ Predicted, data = data2plot_imp_20))

p_imp_20 <-
  ggplot(data2plot_imp_20, aes(Predicted, non_rev)) +
  geom_point(colour = "blue", size = 2) +
  coord_equal() +
  # xlim(c(0, 3.5)) + ylim(c(0, 3.5)) +
  geom_smooth(method = 'lm') +
  labs(title = 'non_rev',
       subtitle = 'Random Forest\n test data\n varImp = 20') +
  ggthemes::theme_tufte()
p_imp_20 <- p_imp_20 + geom_abline(intercept = 0,
                                   slope = 1,
                                   colour = 'red')
p_imp_20

### 15 most important variables

varImp <- importance(rfModel)
importantVars <- names(sort(varImp[ , 1], decreasing = TRUE))[1:15]
X_train_imp <- X_train[ , importantVars]
X_test_imp <- X_test[ , importantVars]

data2model_imp <- cbind(X_train_imp, y_train)

rfModel_imp_15 <- randomForest(non_rev ~ ., data = data2model_imp, importance = TRUE)
rfModel_imp_15

y_predict_imp_15 <- predict(rfModel_imp_15, newdata = X_test_imp) %>%
  data.frame()
colnames(y_predict_imp_15) <- c('Predicted')

data2plot_imp_15 <- cbind(y_test, y_predict_imp_15)

summary(lm(non_rev ~ Predicted, data = data2plot_imp_15))

p_imp_15 <-
  ggplot(data2plot_imp_15, aes(Predicted, non_rev)) +
  geom_point(colour = "blue", size = 2) +
  coord_equal() +
  # xlim(c(0, 3.5)) + ylim(c(0, 3.5)) +
  geom_smooth(method = 'lm') +
  labs(title = 'non_rev',
       subtitle = 'Random Forest\n test data\n varImp = 15') +
  ggthemes::theme_tufte()
p_imp_15 <- p_imp_15 + geom_abline(intercept = 0,
                                   slope = 1,
                                   colour = 'red')
p_imp_15

### 10 most important variables

varImp <- importance(rfModel)
importantVars <- names(sort(varImp[ , 1], decreasing = TRUE))[1:10]
X_train_imp <- X_train[ , importantVars]
X_test_imp <- X_test[ , importantVars]

data2model_imp <- cbind(X_train_imp, y_train)

rfModel_imp_10 <- randomForest(non_rev ~ ., data = data2model_imp, importance = TRUE)
rfModel_imp_10

y_predict_imp_10 <- predict(rfModel_imp_10, newdata = X_test_imp) %>%
  data.frame()
colnames(y_predict_imp_10) <- c('Predicted')

data2plot_imp_10 <- cbind(y_test, y_predict_imp_10)

summary(lm(non_rev ~ Predicted, data = data2plot_imp_10))

p_imp_10 <-
  ggplot(data2plot_imp_10, aes(Predicted, non_rev)) +
  geom_point(colour = "blue", size = 2) +
  coord_equal() +
  # xlim(c(0, 3.5)) + ylim(c(0, 3.5)) +
  geom_smooth(method = 'lm') +
  labs(title = 'non_rev',
       subtitle = 'Random Forest\n test data\n varImp = 10') +
  ggthemes::theme_tufte()
p_imp_10 <- p_imp_10 + geom_abline(intercept = 0,
                                   slope = 1,
                                   colour = 'red')
p_imp_10

### 5 most important variables

varImp <- importance(rfModel)
importantVars <- names(sort(varImp[ , 1], decreasing = TRUE))[1:5]
X_train_imp <- X_train[ , importantVars]
X_test_imp <- X_test[ , importantVars]

data2model_imp <- cbind(X_train_imp, y_train)

rfModel_imp <- randomForest(non_rev ~ ., data = data2model_imp, importance = TRUE)
rfModel_imp

y_predict_imp <- predict(rfModel_imp, newdata = X_test_imp) %>%
  data.frame()
colnames(y_predict_imp) <- c('Predicted')

data2plot_imp <- cbind(y_test, y_predict_imp)

summary(lm(non_rev ~ Predicted, data = data2plot_imp))

p_imp_05 <-
  ggplot(data2plot_imp, aes(Predicted, non_rev)) +
  geom_point(colour = "blue", size = 2) +
  coord_equal() +
  # xlim(c(0, 3.5)) + ylim(c(0, 3.5)) +
  geom_smooth(method = 'lm') +
  labs(title = 'non_rev',
       subtitle = 'Random Forest\n test data\n varImp = 5') +
  ggthemes::theme_tufte()
p_imp_05 <- p_imp_05 + geom_abline(intercept = 0,
                             slope = 1,
                             colour = 'red')
p_imp_05


#### Highly correlated variables

jkl <- DATA5 %>%
  select(-date,-non_rev)
jkl_correlations <- cor(jkl)
highCorr <- findCorrelation(jkl_correlations, cutoff = 0.85)
names(jkl[ , highCorr])

#### deciles

zxc <- DATA5 %>%
  mutate(tranche = ntile(DATA5$non_rev, 20))

table(zxc$tranche)

#### rpart

tree <- rpart(non_rev ~ ., data = data2model_imp)
plot(tree)
rpart.plot(tree)
