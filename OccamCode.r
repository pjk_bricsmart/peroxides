library(tidyverse)
library(magrittr)
library(caret)
library(readxl)
library(DataExplorer)
library(tableplot)
library(ggplot2)
library(ggthemes)
library(janitor)
library(naniar)
library(randomForest)

DATA <- read_xlsx(path = 'tags_for_paul.xlsx', col_names = TRUE) %>%
  rename(Date = X__1) %>%
  clean_names() %>%
  remove_empty(which = c ("rows", "cols"))

DATA <- DATA %>%
  select(
  -f5507_residue,
  -f5507_aq,
  -f5507_tq,
  -f5507_teq,
  -f5507_sum_c_t_thq,
  -f5507_part_coeff,
  -f5507_aa_dp
  )
NAME <- names(DATA)

excludeList01 <- NULL
for (k in 1:length(NAME)) {
  if( length(table(DATA[[k]])) == 1 ) {
    excludeList01 <- c(excludeList01, NAME[[k]])
  }
}
DATA2 <- DATA %>% select(-excludeList01)
NAME2 <- names(DATA2)

excludeList02 <- NULL
for (k in 1:length(NAME2)) {
  n <- DATA2 %>%
    select(date, NAME2[[k]]) %>%
    na.omit()
  if( max(n$date) < max(DATA2$date) ) {
    excludeList02 <- c(excludeList02, NAME2[[k]])
  }
}
DATA3 <- DATA2 %>% select(-excludeList02)
NAME3 <- names(DATA3)

excludeList03 <- NULL
for (k in 1:nrow(DATA3)) {
  if( sum(is.na(DATA3[k,])) != 0 ) {
    excludeList03 <- c(excludeList03, DATA3[k, 1])
  }
}
'%!in%' <- function(x,y)!('%in%'(x,y))
DATA4 <- DATA3 %>% filter(date %!in% excludeList03)

##
write.csv(DATA4, file = 'tags-curated.csv') # save curated dataset
##

set.seed(350)

trainingRows <- createDataPartition(DATA4$non_rev,
                             p = 0.80,
                             groups = 10,
                             list = FALSE)

train <- DATA4[trainingRows, ]
test <- DATA4[-trainingRows, ]

X_train <- train %>%
  select(-date, -non_rev)
y_train <- train %>%
  select(non_rev)

X_test <- test %>%
  select(-date, -non_rev)
y_test <- test %>%
  select(non_rev)

nzv <- nearZeroVar(X_train, freqCut = 100/0) # returns NULL (PJK 2018-11-12)
correlations <- cor(X_train)
corrplot::corrplot(correlations, order = 'hclust')
highCorr <- findCorrelation(correlations, cutoff = 0.85)

X_train <- X_train[ , -highCorr]
X_test <- X_test[ , -highCorr]

correlations <- cor(X_train)
corrplot::corrplot(correlations, order = 'hclust')
comboInfo <- findLinearCombos(X_train) # returns NULL (PJK 2018-11-12)
alles <- cbind(X_train, y_train)
comboInfo <- findLinearCombos(alles) # returns NULL (PJK 2018-11-12)
preProcValues <- preProcess(X_train, method = c('center', 'scale'))
X_train_proc <- predict(preProcValues, X_train)
X_test_proc <- predict(preProcValues, X_test)
trainSet <- cbind(y_train, X_train_proc)
control <- trainControl(
  method = 'repeatedcv',
  number = 5,
  # repeats = 3,
  search = 'random'
)
set.seed(350)

rf <- train(
  non_rev ~ .,
  data = trainSet,
  method = 'rf',
  metric = 'RMSE',
  tuneLength  = 15,
  importance = TRUE,
  trControl = control
)

print(rf)
plot(rf)

peroxide.rf <- randomForest(non_rev ~ .,
                            data = trainSet,
                            importance = TRUE,
                            mtry = 17)

peroxide.importance <- round(importance(peroxide.rf), 2) %>%
  data.frame()

varImpPlot(peroxide.rf)


